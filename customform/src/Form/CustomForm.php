<?php
  /**
   * @file
   * Contains \Drupal\customform\Form\CustomForm
   */
namespace Drupal\customform\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Routing;
use Drupal\Component\Utility\EmailValidator;

 /**
  * Παρέχει μια Custom form.
  */
class CustomForm extends FormBase {

  /**
   * (@inheritDoc)
   */
  protected function getEditableConfigNames() {
    return [
      'custom.custom_form',
    ];
  }

  /**
   * (@inheritDoc)
   */
  public function getFormId() {
      return 'custom_form';
  }

  /**
   * (@inheritDoc)
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['text'] = [
      '#title' => t('Please fill the information below'),
      '#type' => 'label',
    ];
    $form['name'] = [
      '#title' =>  $this->t('Name'),
      '#type' => 'textfield',
      '#size' => 50,
      '#required' => TRUE,
    ];
    $form['email'] = [ 
      '#title' => $this->t('Email'),
      '#type' => 'textfield',
      '#size' => 50,
      '#required' => TRUE,
    ];
    $form['password'] = [
      '#title' =>  $this->t('Password'),
      '#type' => 'password',
      '#size' => 50,
      '#required' => TRUE
    ];
    $form['cancel'] = array(
      '#type'   => 'submit',
      '#value'  => t('Cancel'),
      '#access' => TRUE,
      '#submit' => [
        'callback' => '::cancelForm',
      ],
    );
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#default_value' => $this->t('Submit'),
    ];
    $form['actions']['submit_with_endpoint'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#default_value' => $this->t('Submit With Endpoint'),
    ];

    $form['#theme'] = 'customform';
    $form['#attached']['library'][] = "customform/cf-css";

    return $form;
  }
  /**
   * (@inheritDoc)
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // $value = $form_state->getValue('email');
    // if ($value == !\Drupal::service('email.validator')->isValid($value)) {
    //   $form_state->setErrorByName ('email', t('The email address %mail is not valid', array('%mail' => $value)));
    //   return;
    // }
    // $this->config('custom.custom_form')->save();
    // parent::validateForm($form, $form_state);
  }

  /**
   * (@inheritDoc)
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

  /** 
   * (@inheritDoc)
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function cancelForm(array &$form, FormStateInterface $form_state){
    $redirect = new RedirectResponse(Url::fromUserInput('/')->toString());
    $redirect->send();
  }
}
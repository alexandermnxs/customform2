<?php

namespace Drupal\customform\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\customform\Form\CustomForm;
use Symfony\Component\HttpFoundation\JsonResponse;

class CustomFormController extends ControllerBase {


  public function jsonEndpoint(Request $request){

    $data = json_decode($request->getContent(), true);

    $name = isset($data['name']) ? $data['name'] : null;
    $email = isset($data['email'])  ? $data['email'] : null;
    $password = isset($data['password'])  ? $data['password'] : null;

    \Drupal::logger('customform')->notice(json_encode($data));
    
    if ($name && $email && $password) {

      return new JsonResponse([ 
        'data' => 'Success', 
      ]); 
    }
    else {
      return new JsonResponse([
        'status' => 'failed',
      ], 400);
    }
}
}
(function ($, Drupal, drupalSettings) {
  /**
   * Περιέχει τα actions της φόρμας επεξεργασίας των πεδίων customform 
   */
  Drupal.behaviors.customFormHandler = {
    attach: function (context, setting) {
      // Event listener για το κλικ στο κουμπί Υποβολή της φόρμας customform
      $('#edit-submit', context)
        .once('edit-submit-click')
        .on('click', (event) => {
          event.preventDefault();
          
          const name = $('#edit-name', context).val(); 
          const email = $('#edit-email', context).val(); 
          const password = $('#edit-password', context).val(); 

          console.log(name, email, password);
      });

      // Event listener για το κλικ στο κουμπί Υποβολή με Endpoint της φόρμας customform
      $('#edit-submit-with-endpoint', context)
      .once('edit-submit-with-endpoint-click')
      .on('click', (event) => {
        event.preventDefault();
        
        const name = $('#edit-name', context).val(); 
        const email = $('#edit-email', context).val(); 
        const password = $('#edit-password', context).val(); 

        console.log(name, email, password);
        $.post('/customform2/customform/endpoint', JSON.stringify({name: name, email: email, password: password}));
      });  
    },
  };
})(jQuery, Drupal, drupalSettings);